
import './App.css';
import logo from './images/arcadelogo.png'
import gun from './images/arcadegun.png'
import tv from './images/gametv.png'
import header from './images/header.png'
import brandlg from './images/brandlg.png'


function App() {
  return (
    <div className="App">
        <div className="main-div">
          <div className="revolver">
            <div className="logo">
              <img style ={{ padding : "30px"}} src={logo} width = "270px" ></img>
            </div>
            <div style ={{ display : "flex", justifyContent:"center"}}>
              <img src={gun} width = "370px" ></img>
            </div>
            <div style ={{height:"100%", display : "flex", justifyContent:"left",alignItems:"end"}}>
              <img src={brandlg} width = "170px" height = "170px" ></img>
            </div>
          </div>
          <div className="revolver">
            <div style ={{ display : "flex", justifyContent:"center"}}>
              <img src={header} width = "100%" ></img>
            </div>
            <div style ={{ display : "flex", justifyContent:"center"}}>
              <img src={tv} width = "70%" ></img>
            </div>
          </div>
        </div>
    </div>
  );
}

export default App;
